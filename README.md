>>> muutuja = 5
>>> 5 + 2
7
>>> vastus = 5 + 2
>>> vastus * 3
21
>>> 
>>> vastus2 = vastus * 3


>>> type(5)
<class 'int'>
>>> type('5')
<class 'str'>
>>> arv = 3.647586073
>>> round(arv,3)
3.648
>>> round(arv, 4)
3.6476
>>> int(3.14)
3
>>> 
>>> 6/3
2.0
>>> 6//3
2
>>> "sone"
'sone'
>>> 'sone'
'sone'
>>> sone = "kalamees"
>>> sone[1:5]
'alam'
>>> sone[::3]
'kae'


>>> sone
'kalamees'
>>> sone[-1]
's'
>>> sone[::-1]
'seemalak'
>>> lause = "SEE ON ÜKS LAUSE"
>>> lause.split()
['SEE', 'ON', 'ÜKS', 'LAUSE']


>>> lause.split()
['seexonxüksxlause']
>>> lause.split()
['seexonxüksxlause']
>>> x = " "
>>> lause.split(x)
['seexonxüksxlause']
>>> lause.split('x')
['see', 'on', 'üks', 'lause']
>>> 


>>> mylist = [0, 1, 2, 3, 4]
>>> mylist[2]
2
>>> mylist[:3]
[0, 1, 2]
>>> mylist[::-1]
[4, 3, 2, 1, 0]
>>> tähestik = ['a' , 'b' , 'c']
>>> tähestik
['a', 'b', 'c']
>>> tähestik.append('d')
>>> tähestik
['a', 'b', 'c', 'd']
>>> tähestik + ['e']
['a', 'b', 'c', 'd', 'e']
>>> tähestik[0]
'a'
>>> tähestik.pop()
'd'
>>> tähestik
['a', 'b', 'c']
>>> mylist.reverse()
>>> mylist
[4, 3, 2, 1, 0]
>>> mylist[::-1]
[0, 1, 2, 3, 4]


lause = "seexonxsiinxlause"
eemalda x-d
prindi lause

lause = "seexonxsiinxlause"
temp = lause.split('x')
valjund = ''
for i in temp:
    valjund = valjund + i + " "
print(valjund)

lause = "seexonxsiinxlause"
temp = lause.split("x")
print(*temp)



!!!sõnaraamatud ehk dictionary

dict = {'nimi' : 'Luiza' ,
        'vanus' : '10' ,
        'kool' : 'tlg' ,
        'linn' : 'tallinn'}
print(dict)
{'kool': 'tlg', 'nimi': 'Luiza', 'vanus': '10', 'linn': 'tallinn'}

for key, value in dict.items():
    print(key, "=>", value)
    
linn => tallinn
kool => tlg
nimi => Luiza
vanus => 10

KODUS
3.PTK ÕPIKUST LUGEDA JÄRGMINE KORD TEST MUUTUJAD, NUMBRID, SÕNED, TURTLE, BOOLEAN
ESIMESED 30 MIN KORDAME JA SIIS 15MIN TEST
3.PTK ÕPIKUST lugeda ja kõik harjutsued läbi teha ja bitbucketisse üles laadida kodu2.py nime alla 


///faktoriaal
fact = 1
n = 5
while n > 1:
fact *= n
n = n-1

///lõpmatu tsükkel
i = 0
while(i>10)
print(i)
i = i - 1